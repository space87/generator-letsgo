var assert = require('chai').assert;


describe('addition', function () {
  it('should add 1+1 correctly', function (done) {
    var onePlusOne = 1 + 2;
    assert.equal(onePlusOne, 3, 'Did not equal 3');
    // must call done() so that mocha know that we are... done.
    // Useful for async tests.
    done();
  });
});


describe('subtract', function () {
  it('should subtract 2-1 correctly', function (done) {
    var onePlusOne = 2 - 1;
    assert.equal(onePlusOne, 1, 'Did not equal 1');
    // must call done() so that mocha know that we are... done.
    // Useful for async tests.
    done();
  });
});
