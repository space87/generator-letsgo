var gulp = require('gulp');
var chalk = require('chalk');
var del = require('del');
var stylish = require('jshint-stylish');
var scsslint = require('gulp-scss-lint');
var htmlhint = require("gulp-htmlhint");
var htmlmin = require('gulp-htmlmin');
var sizereport = require('gulp-sizereport');
var gutil = require('gulp-util');
var mocha = require('gulp-mocha');
var babel = require('gulp-babel');
var connect = require('gulp-connect');
var sourcemaps = require('gulp-sourcemaps');
var nunjucks = require('gulp-nunjucks');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var sass = require('gulp-sass');
var size = size = require('gulp-size');
var rename = require('gulp-rename');
var eslint = require('gulp-eslint');
var concat = require('gulp-concat');
var newer = require('gulp-newer');
var webpack = require('webpack-stream');
var envify = require('gulp-envify');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');



// author: jason staerck , github: space87 , space87.github.io


// styles tasks

function styles() {
	return gulp.src('client/scss/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({browsers: ['last 2 versions', 'IE 9']}))
		.pipe(cssnano())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('build/css'))
		.pipe(connect.reload())
}


// scripts tasks

<% if (babel) { %>
	function scripts() {
		var environment = {
		  NODE_ENV: 'production'
		};
		return gulp.src('client/js/main.js')
			 .pipe(envify(environment))
			 .pipe(webpack({
		    watch: false,
				output: {
		      filename: 'bundle.js'
		    },
		    module: {
		    loaders:[
					{
						loader: 'babel',
						query: {
				      presets: ['es2015','react']
				    }
					}
				],

		    }
		  }))
			.pipe(gulp.dest('build/js/'))
			.pipe(connect.reload())
	}
<% } else { %>
	function scripts() {
		return gulp.src('client/js/**/*.js')
			.pipe(concat('bundle.js'))
			.pipe(gulp.dest('build/js'))
			.pipe(connect.reload())
	}
<% } %>

// third party scripts

function copyDevLibs() {
	return gulp.src('client/assets/libs/*')
		.pipe(gulp.dest('build/assets/libs/'))
}

function copyProdLibs() {
	return gulp.src('client/assets/libs/*')
		.pipe(gulp.dest('dist/assets/libs/'))
}

// cleaning tasks

function clean() {
	return del(['build/**']).then(function(paths) {
		console.log(chalk.green("Build folder cleaned"));
	})
}

function cleanDist() {
	return del(['dist/**']).then(function(paths) {
		console.log(chalk.green("Dist folder cleaned"));
	})
}

// js linting

function jsLint() {
	return gulp.src('client/js/**/*.js')
		.pipe(eslint())
		.pipe(eslint.format())
		.on('error', gutil.log);

}

// sass linting

function sassLint() {
	return gulp.src('client/scss/**/*.scss')
		.pipe(scsslint({
    'config': './.scss-lint.yml',
	}))
}

// run local dev server

function runServer() {
	connect.server({
		root: 'build/',
		port: <%= port %>,
		livereload: true
	})
}

// complie html from nunjucks & validate

function complieHTML() {
	return gulp.src('client/templates/index.nunjucks')
		.pipe(nunjucks.compile({name:'<%= name %>'}))
		.pipe(gulp.dest('build/'))
		.pipe(connect.reload())

}

// minify Html

function buildHTML() {
	return gulp.src('build/index.html')
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(gulp.dest('dist/index.html'))
		.on('error', gutil.log)
}

function validateHTML() {
	return gulp.src('build/index.html')
			.pipe(htmlhint())
			.pipe(htmlhint.reporter())
}

// report on file size of product files

function fileSizeReport() {
	return gulp.src('dist/**/*')
		.pipe(sizereport({
			'*':{
				'maxSize': 100000
			}
		}))
}

function fileSize() {
	return gulp.src('build/**/*')
		.pipe(sizereport({
			'*': {
				'maxSize':200000
			}
		}))

}


// minify js

function minifyJS() {
	return gulp.src('build/js/*.js')
    .pipe(uglify())
		.pipe(rename('bundle.min.js'))
    .pipe(gulp.dest('dist/js'))
		.on('error', gutil.log)
}


// minify css

function minifyCSS() {
	return gulp.src('build/css/*.css')
    .pipe(cssnano())
		.pipe(rename('main.min.css'))
    .pipe(gulp.dest('dist/css'))
		.on('error', gutil.log)
}

// minify images

function images()  {
	return gulp.src('build/assets/img/**/*')
	.pipe(newer('dist/assets/img'))
	.pipe(imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}))
	.pipe(gulp.dest('dist/assets/img'))
	.pipe(connect.reload());
}

function devImages() {
	return gulp.src('client/assets/img/**/*')
	.pipe(newer('build/assets/img'))
	.pipe(gulp.dest('build/assets/img'))
	.pipe(connect.reload());
}


// test js

function testJS() {
	return gulp.src('client/tests/test-*.js', { read: false })
    .pipe(mocha({
      reporter: 'spec',
      globals: {
        chai: require('chai')
      }
    }))
		.on('error', gutil.log);
}

// watching
function watcher() {
	gulp.watch('client/scss/**/*.scss', gulp.series(styles));
	gulp.watch('client/js/**/*.js', gulp.series(scripts));
	gulp.watch('client/assets/img/*', gulp.series(devImages,fileSize));
	gulp.watch('client/templates/**/*.nunjucks', gulp.series(complieHTML,validateHTML));
}

function mochaWatcher() {
	gulp.watch('client/tests/*.js', testJS);
}



// Gulp tasks

gulp.task('build', gulp.series(clean,gulp.parallel(scripts,styles),devImages,copyDevLibs,complieHTML,validateHTML,fileSize,gulp.parallel(watcher,runServer)));

gulp.task('production',	gulp.series(cleanDist, buildHTML, images, minifyJS, minifyCSS,copyProdLibs,fileSize,fileSizeReport));

gulp.task('test',	gulp.series(testJS, mochaWatcher));

gulp.task('scsslint',	gulp.series(sassLint));

gulp.task('jslint',	gulp.series(jsLint));



// small tasks that may want to run on there own

gulp.task('default', gulp.series('build'));
