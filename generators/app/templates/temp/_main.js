<% if(babel) { %>
	let ie9 = document.querySelector('body').classList.contains('ie9');
	let GlobalCountry = document.querySelector('head').getAttribute('lang');
	let ua = navigator.userAgent.toLowerCase();
	let isAndroid = ua.indexOf("android") > -1;

	if(isAndroid) {
		document.querySelector('body').classList.add('android');
	}
	
<% } else { %>
	var ie9 = document.querySelector('body').classList.contains('ie9');
	var GlobalCountry = document.querySelector('head').getAttribute('lang');
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1;

	if(isAndroid) {
		document.querySelector('body').classList.add('android');
	}
<% } %>
