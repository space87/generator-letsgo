
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var extend = require('deep-extend');
var mkdirp = require('mkdirp');

module.exports = yeoman.Base.extend({

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the outstanding ' + chalk.red('generator-letsgo') + ' generator!'
    ));

    var prompts = [{
			type	: 'input',
			name	: 'name',
			message	: 'Your project name',
			default	: this.appname // default to current folder name
		}, {
			type	: 'input',
			name	: 'port',
			message	: 'What port would you like your server to run from',
			default	: '3000' // default to current folder name
		}, {
			type	: 'confirm',
			name	: 'react',
			message	: 'Are you using react with this project',
			default: false
		}, {
			type	: 'confirm',
      name	: 'babel',
      message	: 'Do you want to use es6(babel)',
      default: false
		},{
			type	: 'confirm',
      name	: 'notifications',
      message	: 'Do you want to enable system notifications on errors?',
      default: true
		}];

    this.prompt(prompts, function (props) {
      this.props = props;
      // To access props later use this.props.someAnswer;

      done();
    }.bind(this));
  },
	config: function() {
		var pkg = this.fs.readJSON(this.destinationPath('package.json'), {});

		extend(pkg, {
			name:this.appname,
			version:"1.2.2",
			description: "A simple yeoman generator that will help get you setup quickly for a project, It allows you to use scss, es6, react within your project but dosent force you to install every thing. you and pick or choose what is installed and if you prefer to not write in es6 thats fine too. it also covers linting for scss and js and also allows you to write unit testing in mocha and runs it via a gulp task. it has a production step too that will minify all files and images ready for production use.",
			author: {
		    "name": "Jason Staerck",
		    "email": "jason.staerck@gmail.com",
		    "url": "space87.github.io"
		  },
			files:[
				"generators"
			],
			main:"generators\\index.js",
			keywords: [
				"es6",
				"html5",
				"babel",
				"nunjucks",
				"mocha",
				"imagemin",
				"production build",
				"unit testing"
			],
			devDependencies: {
				"babel-core": "^6.7.4",
		    "chalk": "^1.1.3",
		    "del": "^2.2.0",
		    "gulp": "gulpjs/gulp#4.0",
		    "gulp-autoprefixer": "^3.1.0",
		    "gulp-babel": "^6.1.2",
		    "gulp-clean-css": "^2.0.4",
		    "gulp-connect": "^3.2.2",
		    "gulp-eslint": "^2.0.0",
		    "gulp-exclude-gitignore": "^1.0.0",
		    "gulp-htmlhint": "^0.3.1",
		    "gulp-htmlmin": "^1.3.0",
		    "gulp-mocha": "^2.2.0",
		    "gulp-notify": "^2.0.0",
		    "gulp-nunjucks": "^2.2.0",
		    "gulp-plumber": "^1.0.0",
		    "gulp-postcss": "^6.1.0",
		    "gulp-rename": "^1.2.2",
				"gulp-imagemin": "3.0.0",
		    "gulp-sass": "^2.2.0",
		    "gulp-scss-lint": "^0.3.9",
		    "gulp-size": "^2.1.0",
		    "gulp-sizereport": "^1.1.3",
		    "gulp-sourcemaps": "^1.6.0",
		    "gulp-uglify": "^1.5.3",
		    "gulp-util": "^3.0.7",
		    "jshint-stylish": "^2.1.0",
		    "path": "^0.12.7",
		    "susy": "^2.2.12",
				"mkdirp":"^0.5.1",
				"gulp-newer": "1.1.0",
				"deep-extend":"^0.4.1",
		    "vinyl-ftp": "0.4.5",
				"gulp-cached": "^1.1.0",
				"gulp-concat": "^2.6.0",
				"babel-polyfill": "6.7.4",
				"webpack-stream": "3.1.0",
				"chai":"3.5.0",
				"should": ">= 0.0.1",
				"postcss": "5.0.19",
				"autoprefixer":"6.3.6",
				"gulp-cssnano":"2.1.2",
				"gulp-envify":"1.0.0"
			},
			license: "mit"
		});

		if (this.props.react) {
			pkg.devDependencies['react'] = '15.0.1';
			pkg.devDependencies['react-dom'] = '15.0.1';
			pkg.devDependencies['redux'] = '3.5.2';
			pkg.devDependencies['react-router'] = '2.4.0';
		}

		if (this.props.babel) {
			pkg.devDependencies['gulp-babel'] = '^6.1.2';
			pkg.devDependencies['babel-core'] = '^6.7.4';
			pkg.devDependencies['babel-loader'] = '^6.2.4';
      pkg.devDependencies['babel-preset-react'] = '^6.5.0';
			pkg.devDependencies['babel-preset-es2015'] = '^6.6.0';
			pkg.devDependencies['history'] = '2.1.1';
		}


		this.fs.writeJSON(this.destinationPath('package.json'), pkg);

		this.fs.copy(
			this.templatePath('_bowerrc'),
			this.destinationPath('.bowerrc')
		);

		this.fs.copy(
			this.templatePath('gitignore'),
			this.destinationPath('.gitignore')
		);

		this.fs.copy(
			this.templatePath('.scss-lint.yml'),
			this.destinationPath('.scss-lint.yml')
		);

		if (this.props.babel) {
			this.fs.copy(
				this.templatePath('.babelrc'),
				this.destinationPath('.babelrc')
			);
		}

	},

  writing: function() {


		    this.fs.copyTpl(
		      this.templatePath('_gulpfile.js'),
		      this.destinationPath('gulpfile.js'),
					{
						babel: this.props.babel,
						buildDest: '/build',
				 		port: this.props.port,
						name: this.props.name
					}
		    );

				mkdirp.sync(this.destinationPath('client'));
				mkdirp.sync(this.destinationPath('client/assets'));
				mkdirp.sync(this.destinationPath('client/assets/img'));
				mkdirp.sync(this.destinationPath('client/assets/libs'));


				this.directory(
						this.templatePath('_client/scss/'),
						this.destinationPath('client/scss/')
				);

				this.directory(
						this.templatePath('_client/tests/'),
						this.destinationPath('client/tests/')
				);

				// need to add in modules for scss imports
				this.fs.copyTpl(
					this.templatePath('temp/_main.scss'),
					this.destinationPath('client/scss/main.scss'), {
					}
				);


				this.fs.copyTpl(
					this.templatePath('temp/_main.js'),
					this.destinationPath('client/js/main.js'), {
						babel: this.props.babel,
						react: this.props.react
					}
				);

				this.fs.copy(
					this.templatePath('_client/templates/_index.html'),
					this.destinationPath('client/templates/index.html')
				);

				this.fs.copyTpl(
					this.templatePath('_client/templates/_parent.html'),
					this.destinationPath('client/templates/parent.html'), {
						name: this.props.name,
						react: this.props.react,
						babel: this.props.babel
					}
				);

  },

  install: function () {
    this.installDependencies();
  }
});
