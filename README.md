# generator-letsgo [![NPM version][npm-image]][npm-url] [![Dependency Status][daviddm-image]][daviddm-url]
> A simple yeoman generator that will help get you setup quickly for a project, It allows you to use scss, es6, react within your project but dosent force you to install every thing. you and pick or choose what is installed and if you prefer to not write in es6 thats fine too. it also covers linting for scss and js and also allows you to write unit testing in mocha and runs it via a gulp task. it has a production step too that will minify all files and images ready for production use.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-letsgo using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

Make sure you have ruby sass gem installed as well for the scsslinting to work

```bash
npm install -g yo
npm install -g generator-letsgo
```

Generate a new project by running:

```bash
yo letsgo
```

## Getting To generator-letsgo

* Compile scss to css
* Compile es6 to es5
* Image minimization
* Lint your javascript and sass
* Run mocha unit tests
* Development & production tasks
* Third party scripts handled by bower
* Simple express server for development
* Build HTML pages & modules using Nunjucks
* Validate and minify HTML
* Report file sizes during building processes



## Roadmap
i do plan on upgrading and adding future releases to this generator and will be controlled via this [trello board](https://trello.com/b/GH5C2aJL/generator-letsgo)

if you have any requests or ideas please place in side the ideas column or a issue via github and i will look and evaluate it.



## License

MIT license © [Jason Staerck](http://space87.github.io/)


[npm-image]: https://badge.fury.io/js/generator-letsgo.svg
[npm-url]: https://npmjs.org/package/generator-letsgo
[daviddm-image]: https://david-dm.org/space87/generator-letsgo.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/space87/generator-letsgo
